#include <string>
#include <random>
using std::string;

string randDNA(int s, string b, int n)
{
	std::mt19937 eng1(s);
	int min = 0;
	int max = b.size() -1;
	std::uniform_int_distribution<>uniform(min,max);
	int index =0;
	string ATCG = "";
	
	if (b.size() ==0)
	{
		ATCG = "";
		return ATCG;
	}
	for (int i = 0; i < n; i++)
	{
		index = uniform(eng1);
		ATCG = ATCG + b[index];
	}
	return "ATCG";
}
